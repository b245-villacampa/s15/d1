// [Section] Comments

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.

// There are two types of comments:
 // - single line commemnt (Ctrl + / )denoted by two slashes ( // )


/*	-Multiline comment
	- denoted by 
	- Ctrl + shift + /
*/

// [SECTION] Statements and Syntax 

	// Statements 
		// it is the set on instructions that we tell the computer to perform
		// JS statements usually ends with semicolon (;)
			// semicolon is also called as delimeter
		// Semicolons are not required. 
			// it is use to help us train or locate a statement ends.

	// Syntax 
		// In programming, it is the set of rules that describes how statements must be constructed. 

// [SECTION] Variables
	
	//It is used to contain data. (Container).

// Declairing a variable
	// tells oir devices taht a variable name is created abd is ready to stote data.

	// Syntax: let / cons variableName;
		// let is a keyword taht is usually used in declaring a variable.
		 let myVariable;

		 console.log(myVariable); // useful for printing values of a variables.
		 // Initialize a value 
		 	// Storing the initial/starting value of a variable.
		 	// Assignment Operators (=)
		myVariable = "Hello";

		// Reassignment a variable value
			// Changing the initial value to a new value.
		myVariable - "Hello World";
		console.log(myVariable);
		 	 
		// const
			// "const" is used to declare and initialized a constant variable. 
			//A "constant" variable does not change. 

		// const PI;
		// PI = 3.1416;
		// console.log(PI); // error; Initial value is missing for const.

	// Declaring with Initialization 
		//a variable is given it's initial or starting value upon declaration.
			// Syntax: let/const vatiableName = value;

			// let keyword
			let productName = "Desktop Computer";
			console.log(productName);

			let productPrice = 18999; //let should be declare once 
			productPrice = 20000;
			console.log(productPrice);

			// const keyword (DI NA NABABAGO)
			const PI = 3.1416;
			console.log(PI);

/*
	Guide in Writing Variables.
	1. Use the "let" keyword if the variable will contain different values / can change over time.
		Naming Convention: variable name should starts with "lower characters" and "camelCasing" is used for multiple words.
	2. Use the "const" keyword if the variable does not changed.
		Naming Convention: "const" keyword should be followed by ALL CAPITAL VARIABLE NAME (single value variable).
	3. Variable names should be indicative (or descriptive) of the value being store to avoid confusion.
	4. Variable names are "case sensitive".
*/

// Multiple variable declaration

		let productCode = "DC017", productBrand = "Dell";
		console.log(productCode, productBrand);

// Using a reserved keyword
		// const let = "hello";
		// console.log(let); error: lexically bound is disallowed.

// [SECTION] Data Types 

//In Javascript, there are six types of data
	// String 
		// series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// Strings in JavaScript can be written using a single quote ('') or double quote ("").

		let country = "Philippines";
		let province = 'Metro Manila';

		//Concatenating Strings 
			//Multiple string values that can be combined to create a single string using the "+" symbol.
		let greeting = "I live in " + province + ", " + country;
		console.log(greeting);

		// Escape Characters
		// (\) in string combination with other characters can produce different result 
			// "\n" refers to creating a new line
		let mailAddress = "Quezon City\nPhilippines"
		console.log(mailAddress);

		// Expected output: John's employees went home early.
		let message = "John's employees went home early.";
		console.log(message);

		message = 'Jane\'s employees went home early.';
		console.log(message);

	// Numbers
		//Positives, negatives or numbers with decimal places

		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/ Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine numbers and screen

		console.log("Jonh's grade last quarter is " + grade); 

	// Boolean
		// Boolean values are ormally used to store values realating to the state of certain things.
		//  true or false (two logical values)

		let isMarried = false;
		let isGoodConduct = true;

		console.log("isMarried " + isMarried);
		console.log("isGoodConduct " + isGoodConduct);

	// Arrays
		// are special kind of data type that can use to store multiple related values. 
		
		//Syntax: let/cost arrayName = [elementA, elementB, ... elementNth];

		const grades = [98.7, 92.1, 90.2, 94.6];
		// constant variable cannot be reaassigned.
		// grades = 100;

		// changing the elements of an array or changing the properties of am object is allowed in constant variable.
		grades[0] = 100;
		console.log(grades);

		//This will work butr not recommended
		let details = ["John", "Smith", 32, true];
		console.log(details);

	//Objects
		//objects are another special kind of data type that is used to mimic real world objects/items 
		// used to create complex data that contains information relavant to each other.
		// key-value pairs
		/*
			Syntax: 
			let/const object = {
				propertyA: valueA,
				propertyB: valueB
			}
			
		*/

		let person ={
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false, 
			contact: false,
			contact:["+63912 345 6789", "8123 456"],
			address: {
				houseNumber :"345",
				city:"Manila"
			}
		};

		console.log(person);

		// They're are also useful for creating abstract objects 
		let myGrades={
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		};

		console.log(myGrades);

		// Type of operator
			//is used to determines the type of data or the value of a variable. 

		console.log(typeof myGrades); //object

		// arrays is a special type of object with methods
		console.log(typeof grades); // object

	// Null
		// indicates the absence of a value
		let spouse = null;
		console.log(spouse);

	// Undefined
		// indicates that a variables has not been given a value yet 
		let fullName;
		console.log(fullName);

